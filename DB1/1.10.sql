set @losers_name = (select name from (SELECT name 
FROM epam.marks
inner join epam.student using(student_id)
where mark = 2
group by name 
having count(*) = 3) n);

delete from epam.student 
where name = @losers_name;